﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeyboard : MonoBehaviour {

	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	float speed = 10.0f;
	void FixedUpdate() {
		Vector3 rigidBodyVelocity = rigidbody.velocity;
		rigidBodyVelocity.x = Input.GetAxis ("Horizontal") * speed;
		rigidBodyVelocity.z = Input.GetAxis("Vertical") * speed;

		rigidbody.velocity = rigidBodyVelocity;
	}
}
