﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AiPaddle : MonoBehaviour {
	
	public GameObject puck;
	public GameObject goal;
	public float speed = 1f;

	private Rigidbody rigidbody;
	private enum State { MOVE_TO_CORRECT_ANGLE, MOVE_TO_GOAL, WAIT }
	private State state = State.WAIT;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}

	void FixedUpdate () {
		if (state == State.WAIT) {
			StartCoroutine (Wait ());
			return;
		}

		Vector3 puckPos = puck.transform.position;
		Vector3 goalPos = goal.transform.position;

		float angle = Mathf.Atan2(puckPos.z-goalPos.z, puckPos.x-goalPos.x);
		Vector3 targetPos = new Vector3(puckPos.x + Mathf.Cos (angle), rigidbody.position.y, puckPos.z + Mathf.Sin (angle));

		if ((rigidbody.position - targetPos).magnitude < 0.5) {
			state = State.MOVE_TO_GOAL;
		}

		Vector3 dir = new Vector3();
		if (state == State.MOVE_TO_CORRECT_ANGLE) {
			dir = targetPos - rigidbody.position;
		} else if (state == State.MOVE_TO_GOAL) {
			dir = puckPos - rigidbody.position;
		}

		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (Mathf.Abs(move) > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.Equals(puck)) state = State.WAIT;
	}

	IEnumerator Wait() {
		yield return new WaitForSeconds (1);
		state = State.MOVE_TO_CORRECT_ANGLE;
		StopAllCoroutines ();
	}
}
